package vn.com.mobile.kit.api;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Api {
    @FormUrlEncoded
    @POST("schedule/guest")
    Call<Feed> getData(
            @Field("studentAccount") String studentAccount,
            @Field("studentPassword") String studentPassword
    );
}
