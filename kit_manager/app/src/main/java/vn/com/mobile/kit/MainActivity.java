package vn.com.mobile.kit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import vn.com.mobile.kit.Fragments.AddScheduleFragment;
import vn.com.mobile.kit.Fragments.NotificationsFragment;
import vn.com.mobile.kit.Fragments.Schedule.ScheduleFragment;
import vn.com.mobile.kit.Fragments.TimeSheetFragment;
import vn.com.mobile.kit.Fragments.UserFragment;

public class MainActivity extends AppCompatActivity {
    private ActionBar toolbar;
    private BottomNavigationView mBottomNavigationView;
    private FrameLayout mFrameLayout;


    //fragments
    private ScheduleFragment mScheduleFragment;
    private AddScheduleFragment mAddScheduleFragment;
    private TimeSheetFragment mTimeSheetFragment;
    private NotificationsFragment mNotificationsFragment;
    private UserFragment mUserFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBottomNavigationView = findViewById(R.id.bottom_navigation);
        mFrameLayout = findViewById(R.id.framelayout);

        //Fragment initializion
        mScheduleFragment = new ScheduleFragment();
        mAddScheduleFragment= new AddScheduleFragment();
        mTimeSheetFragment = new TimeSheetFragment();
        mNotificationsFragment = new NotificationsFragment();
        mUserFragment = new UserFragment();

        InitializeFragment(mScheduleFragment);
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.navigation_schedule: // khi click vao icon schedule
                        InitializeFragment(mScheduleFragment);
                        return true;
                    case R.id.navigation_add_schedule: // khi click vao icon schedule
                        InitializeFragment(mAddScheduleFragment);
                        return true;
                    case R.id.navigation_timesheet: // khi click vao icon schedule
                        InitializeFragment(mTimeSheetFragment);
                        return true;
                    case R.id.navigation_notifications: // khi click vao icon schedule
                        InitializeFragment(mNotificationsFragment);
                        return true;
                    case R.id.navigation_user: // khi click vao icon schedule
                        InitializeFragment(mUserFragment);
                        return true;

                }
                return false;
            }
        });
    }

    private void InitializeFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.framelayout,fragment);
        fragmentTransaction.commit();
    }

}