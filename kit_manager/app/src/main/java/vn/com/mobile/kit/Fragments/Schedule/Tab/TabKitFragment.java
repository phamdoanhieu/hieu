package vn.com.mobile.kit.Fragments.Schedule.Tab;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vn.com.mobile.kit.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabKitFragment extends Fragment {

    public TabKitFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab_kit, container, false);
    }
}
