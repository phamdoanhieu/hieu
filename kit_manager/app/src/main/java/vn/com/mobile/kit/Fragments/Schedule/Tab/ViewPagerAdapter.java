package vn.com.mobile.kit.Fragments.Schedule.Tab;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private int numberOfTabs;
   // private Context mContext;

    public ViewPagerAdapter(@NonNull FragmentManager fm, int numberOfTabs ) {
        super(fm);
        this.numberOfTabs= numberOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new TabAllFragment();
            case 1:
                return new TabKmaFragment();
            case 2:
                return new TabKitFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
