package vn.com.mobile.kit.api;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import vn.com.mobile.kit.R;
import vn.com.mobile.kit.api.dataJson.LessonElement;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private Context mContext;
    private List<LessonElement> lessonElements;

    public RecyclerAdapter(Context mContext, List<LessonElement> lessonElements) {
        this.mContext = mContext;
        this.lessonElements = lessonElements;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View tkbView = inflater.inflate(R.layout.item_tkb, parent, false);
        ViewHolder viewHolder = new ViewHolder(tkbView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LessonElement lessonElement = lessonElements.get(position);
        if(lessonElement == null)
        {
            Log.e("RecAdapter", "Ko có ngày học");
        }
        else {
            String lesson = lessonElement.getLesson();
            String subjectName = lessonElement.getsubject();
            String address = lessonElement.getAddress();
            //holder.tv_lesson.setText("Tiết học: "+lesson);
            holder.tv_lesson.setText("Thời gian: " + lesson);
            holder.tv_subjectName.setText("Môn học: " + subjectName);
            holder.tv_address.setText("Địa điểm: " + address);
        }

    }

    @Override
    public int getItemCount() {
        return lessonElements == null ? 0 : lessonElements.size();

    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        private TextView tv_lesson;
        private TextView tv_subjectName;
        private TextView tv_address ;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_lesson = itemView.findViewById(R.id.tv_lesson);
            tv_subjectName = itemView.findViewById(R.id.tv_subjectName);
            tv_address = itemView.findViewById(R.id.tv_address);

        }
    }

}
