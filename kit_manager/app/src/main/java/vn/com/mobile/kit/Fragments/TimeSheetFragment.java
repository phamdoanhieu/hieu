package vn.com.mobile.kit.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vn.com.mobile.kit.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class TimeSheetFragment extends Fragment {

    public TimeSheetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_time_sheet, container, false);
    }
}
