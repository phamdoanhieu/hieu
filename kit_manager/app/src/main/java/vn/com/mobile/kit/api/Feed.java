package vn.com.mobile.kit.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;


import vn.com.mobile.kit.api.dataJson.LessonElement;

public class Feed {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("dataJson")
    @Expose
    private Map<Long,List<LessonElement>> dataJson;

    public Feed(String message, Boolean status, Map<Long, List<LessonElement>> dataJson) {
        this.message = message;
        this.status = status;
        this.dataJson = dataJson;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Map<Long, List<LessonElement>> getDataJson() {
        return dataJson;
    }

    public void setDataJson(Map<Long, List<LessonElement>> dataJson) {
        this.dataJson = dataJson;
    }

    @Override
    public String toString() {
        return "Feed{" +
                "message='" + message + '\'' +
                ", status=" + status +
                ", dataJson=" + dataJson +
                '}';
    }
}
