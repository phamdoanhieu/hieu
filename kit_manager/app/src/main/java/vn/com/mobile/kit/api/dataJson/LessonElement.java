package vn.com.mobile.kit.api.dataJson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LessonElement {

    @SerializedName("lesson")
    @Expose
    private String lesson;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("address")
    @Expose
    private String address;

    public LessonElement(String lesson, String subject, String address) {
        this.lesson = lesson;
        this.subject = subject;
        this.address = address;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getLesson() {
        return lesson;
    }

    public String getsubject() {
        return subject;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "LessonElement{" +
                "lesson='" + lesson + '\'' +
                ", subject='" + subject + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
