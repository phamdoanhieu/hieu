package vn.com.mobile.kit.Fragments.Schedule;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.com.mobile.kit.Fragments.Schedule.Tab.TabAllFragment;
import vn.com.mobile.kit.Fragments.Schedule.Tab.ViewPagerAdapter;
import vn.com.mobile.kit.R;
import vn.com.mobile.kit.api.Feed;
import vn.com.mobile.kit.api.RetrofitClient;
import vn.com.mobile.kit.api.dataJson.LessonElement;

;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleFragment extends Fragment {
    final String TAG = "ScheduleFragment";
    private Button btn_preMonth;
    private Button btn_nextMonth;
    private CompactCalendarView compactCalendarView;
    private TextView tv_month;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
    private Long differentialTime = 39600000L;
    private Gson gson = new Gson();

    private Feed feed;
    public ScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_schedule, container, false);


    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onStart()
    {
        super.onStart();
        final View view = getView();

        /// tabbar
        TabLayout tabLayout = view.findViewById(R.id.tabBar);

        final ViewPager viewPager = view.findViewById(R.id.viewpage);
        final ViewPagerAdapter mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(),tabLayout.getTabCount() );

        viewPager.setAdapter(mViewPagerAdapter);

        if(view != null)
        {
            GetDataWithAPI();
            try {
                feed=JsonToObj();
                Log.e("Hieu","Json from file: " + feed.toString());

            } catch (NullPointerException e) {
                e.printStackTrace();
            }


            btn_preMonth = view.findViewById(R.id.btn_preMonth);
            btn_nextMonth =  view.findViewById(R.id.btn_nextMonth);
            tv_month = view.findViewById(R.id.tv_month);

            compactCalendarView = view.findViewById(R.id.compactcalendar_view);
            compactCalendarView.setUseThreeLetterAbbreviation(true);
            compactCalendarView.shouldDrawIndicatorsBelowSelectedDays(true);
            compactCalendarView.displayOtherMonthDays(false);
            Long millis=System.currentTimeMillis();
            writeToFile(String.valueOf(millis),getContext(),"test.json");
            Log.e("Hieu","milis " + millis);
            //show event
            ShowEvent(feed);

            tv_month.setText(dateFormat.format(compactCalendarView.getFirstDayOfCurrentMonth()));

            compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {


                @Override
                public void onDayClick(Date dateClicked) {
                    Context context = view.getContext();
                    //Api time - dateclick = diferent time
                    Long dateClicked_long = dateClicked.getTime()-differentialTime;

                    try {

                        writeToFile(dateClicked_long.toString(), Objects.requireNonNull(getContext()),"test.json");
                        Log.e("Save", "Da luu date click"+ dateClicked_long.toString()+" vao test.json");
                        mViewPagerAdapter.notifyDataSetChanged();


                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onMonthScroll(Date firstDayOfNewMonth) {

                    tv_month.setText(dateFormat.format(firstDayOfNewMonth));
                }
            });
            btn_preMonth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    compactCalendarView.scrollLeft();
                }
            });

            btn_nextMonth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    compactCalendarView.scrollRight();
                }
            });

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());


                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
    }
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.ENGLISH);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
    public Boolean CompareDate(String str_date1, String str_date2)
    {
        try {


            Date date1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                    .parse(str_date1);
            Date date2 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                    .parse(str_date2);
            if(date1.compareTo(date2) == 0)
            {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void GetDataWithAPI()
    {
        Call<Feed> call = RetrofitClient.getInstance().getApi().getData("CT030419","df8c870d95bd29774d30f25865a69161");
        call.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                if(response.body().getStatus()== false)
                {
                    Log.e("ERROR FROM API: ", "Maybe wrong user or pass, api error");

                }
                else {
                    String jsonInString = gson.toJson(response.body());
                    writeToFile(jsonInString, Objects.requireNonNull(getContext()),"schedule.json");

                    Log.e(TAG, "Response json: " + jsonInString);
                }
//                mLessonElements.addAll(response.body().getSchedule().get(0).getLessons());
//                mRecyclerAdapter = new RecyclerAdapter(MainActivity.this,mLessonElements);
//                mRecyclerView.setAdapter(mRecyclerAdapter);

                Log.e(TAG, "Response code: " + response.code());
                Log.e(TAG, "Response body: " + response.body());

                // Log.e(TAG, "Response read: " + read);

            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t);
            }
        });
    }
    private void writeToFile(String data, Context context,String name) {
        try {
            Log.e(TAG,"File location: " + context.getFilesDir());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(name, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();

        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String readFromFile(Context context,String name) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(name);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    public Feed JsonToObj()
    {
        String read = readFromFile(Objects.requireNonNull(getContext()),"schedule.json");
        Gson gson = new Gson();
        // 1. JSON file to Java object
        return gson.fromJson(read,Feed.class);
    }

    private void ShowEvent(Feed feed)
    {
        Set<Long> set = feed.getDataJson().keySet();
        for (Long time : set) {

            Event ev1 = new Event(Color.WHITE,time+differentialTime,"");
            compactCalendarView.addEvent(ev1);
        }

    }
}
