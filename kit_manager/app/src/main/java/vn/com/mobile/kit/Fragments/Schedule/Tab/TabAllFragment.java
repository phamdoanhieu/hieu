package vn.com.mobile.kit.Fragments.Schedule.Tab;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.com.mobile.kit.R;
import vn.com.mobile.kit.api.Feed;
import vn.com.mobile.kit.api.RecyclerAdapter;
import vn.com.mobile.kit.api.RetrofitClient;
import vn.com.mobile.kit.api.dataJson.LessonElement;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class TabAllFragment extends Fragment {


    private final static String TAG = "TabALl fragment: ";
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mRecyclerAdapter;
    private List<LessonElement> mLessonElements= new ArrayList<>();
    private View view;
    private Context context;
    private Feed feed;
    private Long dateClicked;
    public TabAllFragment(){
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab_all, container, false);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        view = getView();
        context = view.getContext();
        String test= (readFromFile(context,"test.json"));
        test = test.trim();
        dateClicked = Long.parseLong(test);
        feed=JsonToObj();
        Log.e("testtt", test);
        Gson gson = new Gson();

        RecyclerViewProcedure();

    }
    public void RecyclerViewProcedure() {
        mRecyclerView = view.findViewById(R.id.rv_items);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        if(feed.getDataJson().get(dateClicked) == null)
        {
            Log.e("taball ", "khong co event");
        }
        else {
            TextView tv_no_event = view.findViewById(R.id.tv_no_event);
            tv_no_event.setVisibility(View.GONE);
            mLessonElements = new ArrayList<>(Objects.requireNonNull(feed.getDataJson().get(dateClicked)));
            Sort_Lesson(mLessonElements);
            //đưa dữ liệu vào Adapter
            Log.e("produce", mLessonElements.toString());
            mRecyclerAdapter = new RecyclerAdapter(context, mLessonElements);
            mRecyclerView.setAdapter(mRecyclerAdapter);
        }
    }



    private String readFromFile(Context context,String name) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(name);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    public Feed JsonToObj()
    {
        String read = readFromFile(Objects.requireNonNull(getContext()),"schedule.json");
        Gson gson = new Gson();
        // 1. JSON file to Java object
        return gson.fromJson(read,Feed.class);
    }

    public int getFistLesson(String str)
    {
        String[] newStr = str.split(",");
        String First = newStr[0];
        return Integer.parseInt(First);
    }
    public Boolean LessonCmp(String s1, String s2)
    {
        if(getFistLesson(s1)>getFistLesson(s2))
            return true;
        return false;
    }
    public void Sort_Lesson(List<LessonElement> mLessonElements)
    {
        for(int i=0;i <mLessonElements.size(); i++)
        {
            for (int j=i+1; j<mLessonElements.size(); j++)
            {
                if(LessonCmp(mLessonElements.get(i).getLesson(),mLessonElements.get(j).getLesson()))
                {
                    Collections.swap(mLessonElements,i,j);
                }
            }
        }
    }
}
